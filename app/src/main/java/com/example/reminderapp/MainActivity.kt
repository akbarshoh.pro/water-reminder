package com.example.reminderapp

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.example.reminderapp.presentor.components.WatterBottle
import com.example.reminderapp.ui.theme.ReminderAppTheme
import com.example.reminderapp.worker.WorkerHelper
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import java.util.concurrent.TimeUnit

class MainActivity : ComponentActivity() {
    @SuppressLint("InlinedApi")
    @OptIn(ExperimentalPermissionsApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val periodic15TimeRequest =
            PeriodicWorkRequestBuilder<WorkerHelper>(15L, TimeUnit.MINUTES, 5L, TimeUnit.MINUTES)
                .build()

        val periodic30TimeRequest =
            PeriodicWorkRequestBuilder<WorkerHelper>(30L, TimeUnit.MINUTES, 5L, TimeUnit.MINUTES)
                .build()

        val periodicHourTimeRequest =
            PeriodicWorkRequestBuilder<WorkerHelper>(1L, TimeUnit.HOURS, 5L, TimeUnit.MINUTES)
                .build()

        setContent {
            ReminderAppTheme {
                val permissionState =
                    rememberPermissionState(permission = Manifest.permission.POST_NOTIFICATIONS)

                val lifeCicleOwner = LocalLifecycleOwner.current
                DisposableEffect(key1 = lifeCicleOwner) {
                    val observer = LifecycleEventObserver { _, event ->

                        if (event == Lifecycle.Event.ON_RESUME) {
                            permissionState.launchPermissionRequest()
                        }
                    }
                    lifeCicleOwner.lifecycle.addObserver(observer)
                    onDispose { lifeCicleOwner.lifecycle.removeObserver(observer) }
                }

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {

                    var usedWaterAmount by remember {
                        mutableIntStateOf(0)
                    }

                    val totalWaterAmount = remember {
                        2000
                    }

                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(Color(0xFF8E9BFF))
                    ) {

                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(56.dp)
                                .background(color = Color(0xFF5F72FF)),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = "Water reminder",
                                color = Color.White,
                                fontSize = 24.sp
                            )
                        }

                        Text(
                            text = "Choose a time to mention",
                            fontSize = 20.sp,
                            color = Color.White,
                            modifier = Modifier
                                .align(Alignment.TopCenter)
                                .padding(top = 90.dp)
                        )
                        Row(
                            modifier = Modifier
                                .align(Alignment.TopCenter)
                                .padding(top = 140.dp)
                        ) {
                            Button(
                                modifier = Modifier
                                    .height(48.dp)
                                    .width(100.dp),
                                onClick = {
                                    WorkManager.getInstance(this@MainActivity)
                                        .enqueue(periodic15TimeRequest)
                                },
                                colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF5F72FF))
                            ) {
                                Text(text = "15 min")
                            }

                            Button(
                                modifier = Modifier
                                    .padding(horizontal = 16.dp)
                                    .height(48.dp)
                                    .width(100.dp),
                                onClick = {
                                    WorkManager.getInstance(this@MainActivity)
                                        .enqueue(periodic30TimeRequest)
                                },
                                colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF5F72FF))
                            ) {
                                Text(text = "30 min")
                            }

                            Button(
                                modifier = Modifier
                                    .width(100.dp)
                                    .height(48.dp),
                                onClick = {
                                    WorkManager.getInstance(this@MainActivity)
                                        .enqueue(periodicHourTimeRequest)
                                },
                                colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF5F72FF))
                                ) {
                                Text(text = "60 min")
                            }
                        }


                        WatterBottle(
                            totalWaterAmount = totalWaterAmount,
                            unit = "ml",
                            usedWaterAmount = usedWaterAmount,
                            modifier = Modifier.align(Alignment.Center)
                        )
                        Spacer(modifier = Modifier.height(20.dp))

                        Text(
                            text = "Total amount: $totalWaterAmount ml",
                            textAlign = TextAlign.Center,
                            color = Color.White,
                            fontSize = 24.sp,
                            modifier = Modifier
                                .align(Alignment.BottomCenter)
                                .padding(bottom = 150.dp)
                        )

                        Button(
                            onClick = {
                                if (usedWaterAmount >= 2000) {
                                    usedWaterAmount = 2000
                                } else {
                                    usedWaterAmount += 200
                                }
                            },
                            colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF5F72FF)),
                            modifier = Modifier
                                .align(Alignment.BottomCenter)
                                .padding(bottom = 56.dp)
                                .width(140.dp)
                                .height(48.dp)
                        ) {
                            Text(
                                text = "DRINK",
                                fontSize = 20.sp
                            )
                        }

                    }
                }
            }
        }
    }
}

